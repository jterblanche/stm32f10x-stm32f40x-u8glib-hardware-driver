/*

  @file
  @author J. Terblanche
  @version 1.0.0

  @section DESCRIPTION
  u8g_stm32_helper.c

  STM32 Port of the hardware abstraction functions of the u8glib graphics library.

  NOTES:
  	  See the readme.txt file for implementation notes.

  @section LICENSE

  Copyright (c) 2013, J. Terblanche.
  Portions Copyright (c) 2011, olikraus@gmail.com
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice, this list
    of conditions and the following disclaimer.

  * Redistributions in binary form must reproduce the above copyright notice, this
    list of conditions and the following disclaimer in the documentation and/or other
    materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


*/

#include "u8g_stm32_helper.h"
#if defined (STM32F40_41xxx)
	#include "stm32f4xx_conf.h"
#else
	#include "stm32f10x_conf.h"
#endif

extern void Delay_Microseconds(__IO uint32_t nTime);

#ifdef U8G_USE_POWER_CONTROL_FUNCTIONS
	#if U8G_USE_POWER_CONTROL_FUNCTIONS != false
		#define U8G_USE_PWR_CTL_FUNCS
		extern void U8G_Device_PowerDown();
		extern void U8G_Device_PowerUp();
	#endif
#endif

/*
 * @brief Delay execution by nTime milliseconds.
 * @param nTime: Number of milliseconds to delay.
 * @retval None
 */
void u8g_Delay(uint16_t nTime)
{
	Delay_Microseconds(1000 * nTime);
}

/*
 * @brief Delay execution for 1 microsecond.
 * @retval None
 */
void u8g_MicroDelay()
{
	Delay_Microseconds(1);
}

/*
 * @brief Delay execution for 10 microseconds.
 * @retval None
 */
void u8g_10MicroDelay()
{
	Delay_Microseconds(10);
}

/*
 * @brief Pull the SPI slave select line low (enable communications with the device).
 * @retval None
 */
void U8GHelper_SPI_CS_Low()
{
	GPIO_ResetBits(U8GHELPER_SPI_GPIO_PORT, U8GHELPER_SPI_CS_PIN);
}

/*
 * @brief Pull the SPI slave select line high (disable communications with the device).
 * @retval None
 */
void U8GHelper_SPI_CS_High()
{
	GPIO_SetBits(U8GHELPER_SPI_GPIO_PORT, U8GHELPER_SPI_CS_PIN);
}

/*
 * @brief Initialize the GPIO pins and SPI peripheral.
 * @note Configuation options can be customized in u8g_stm32_helper.h
 * @retval None
 */
void U8GHelper_SPI_Init()
{
	U8GHELPER_SPI_GPIO_CLOCKCMD(U8GHELPER_SPI_GPIO_CLOCKBIT, ENABLE);
	U8GHELPER_RESET_GPIO_CLOCKCMD(U8GHELPER_RESET_GPIO_CLOCKBIT, ENABLE);
	U8GHELPER_DNC_GPIO_CLOCKCMD(U8GHELPER_DNC_GPIO_CLOCKBIT, ENABLE);
	U8GHELPER_SPI_CLOCKCMD(U8GHELPER_SPI_CLOCKBIT, ENABLE);

	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Pin = U8GHELPER_RESET_PIN;
#if defined (STM32F40_41xxx)
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
#else
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
#endif
	GPIO_Init(U8GHELPER_RESET_GPIO_PORT, &GPIO_InitStruct);
	GPIO_SetBits(U8GHELPER_RESET_GPIO_PORT, U8GHELPER_RESET_PIN);

	GPIO_InitStruct.GPIO_Pin = U8GHELPER_DNC_PIN;
	GPIO_Init(U8GHELPER_DNC_GPIO_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.GPIO_Pin = U8GHELPER_SPI_CS_PIN;
	GPIO_Init(U8GHELPER_SPI_GPIO_PORT, &GPIO_InitStruct);
	U8GHelper_SPI_CS_High();

#if defined (STM32F40_41xxx)
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
#else
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
#endif
	GPIO_InitStruct.GPIO_Pin = U8GHELPER_SPI_CLK_PIN | U8GHELPER_SPI_MOSI_PIN;
	GPIO_Init(U8GHELPER_SPI_GPIO_PORT, &GPIO_InitStruct);

#if defined (STM32F40_41xxx)
	GPIO_PinAFConfig(U8GHELPER_SPI_GPIO_PORT, U8GHELPER_SPI_CLK_PINSOURCE, U8GHELPER_SPI_GPIO_AF);
	GPIO_PinAFConfig(U8GHELPER_SPI_GPIO_PORT, U8GHELPER_SPI_MOSI_PINSOURCE, U8GHELPER_SPI_GPIO_AF);
#else
//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
#endif

	SPI_InitTypeDef SPI_InitStruct;
	SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;
	SPI_InitStruct.SPI_CPHA = SPI_CPHA_1Edge;
	SPI_InitStruct.SPI_CPOL = SPI_CPOL_Low;
	SPI_InitStruct.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStruct.SPI_Direction = SPI_Direction_1Line_Tx;
	SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStruct.SPI_Mode = SPI_Mode_Master;
	SPI_InitStruct.SPI_NSS = SPI_NSS_Soft;
	SPI_Init(U8GHELPER_SPI_PERIPH, &SPI_InitStruct);

	SPI_Cmd(U8GHELPER_SPI_PERIPH, ENABLE);
}

/*
 * @brief Send a byte over the SPI interface to the device.
 * @param data: the byte of data to send
 * @retval None
 */
void U8GHelper_SPI_Send(uint8_t data)
{
	SPI_I2S_SendData(U8GHELPER_SPI_PERIPH, (uint8_t)data);
#if defined (STM32F40_41xxx)
	while (SPI_I2S_GetFlagStatus(U8GHELPER_SPI_PERIPH, SPI_FLAG_TXE) == RESET);
#else
	while (SPI_I2S_GetFlagStatus(U8GHELPER_SPI_PERIPH, SPI_I2S_FLAG_TXE) == RESET);
#endif
	while (SPI_I2S_GetFlagStatus(U8GHELPER_SPI_PERIPH, SPI_I2S_FLAG_BSY) != RESET);
}


uint8_t u8g_com_hw_spi_fn(u8g_t *u8g, uint8_t msg, uint8_t arg_val, void *arg_ptr)
{
	switch(msg)
	{
		case U8G_COM_MSG_STOP:
			// Stop the device.
			#ifdef U8G_USE_PWR_CTL_FUNCS
				U8G_Device_PowerDown();
			#endif
			break;

		case U8G_COM_MSG_INIT:
			// Initialise GPIO's and the SPI interface.
			#ifdef U8G_USE_PWR_CTL_FUNCS
				U8G_Device_PowerUp();
			#endif
			U8GHelper_SPI_Init();
			u8g_MicroDelay();
			break;

		case U8G_COM_MSG_ADDRESS:
			// Switch between DATA and COMMAND mode.
			u8g_10MicroDelay();
			GPIO_WriteBit(U8GHELPER_DNC_GPIO_PORT, U8GHELPER_DNC_PIN, arg_val);
			u8g_10MicroDelay();
			break;

		case U8G_COM_MSG_CHIP_SELECT:
			if ( arg_val == 0 )
			{
				// Disable SPI communication with the device.
				// This delay is required to avoid that the display is switched off too early.
				Delay_Microseconds(50);
				U8GHelper_SPI_CS_High();
			}
			else if (arg_val == 1)
			{
				// Enable SPI communication with the device.
				U8GHelper_SPI_CS_Low();
			}
			u8g_MicroDelay();
			break;

		case U8G_COM_MSG_RESET:
			// Toggle the reset pin on the device.
			GPIO_WriteBit(U8GHELPER_RESET_GPIO_PORT, U8GHELPER_RESET_PIN, arg_val);
			u8g_10MicroDelay();
			break;

		case U8G_COM_MSG_WRITE_BYTE:
			// Write a byte to the display device.
			U8GHelper_SPI_Send(arg_val);
			u8g_MicroDelay();
			break;

		case U8G_COM_MSG_WRITE_SEQ:
		case U8G_COM_MSG_WRITE_SEQ_P:
			{
			// Write a sequence of bytes to the display device.
			register uint8_t *ptr = arg_ptr;
			while (arg_val-- > 0)
			{
				U8GHelper_SPI_Send(*ptr++);
			}

			u8g_MicroDelay();
			}
			break;

	}
	return 1;
}
