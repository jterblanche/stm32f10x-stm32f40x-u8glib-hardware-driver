STM32 support for u8glib
------------------------
This project adds STM32 support for the popular u8glib graphics library.
See the u8glib page at https://code.google.com/p/u8glib/ for a list of supported
hardware. Many thanks to the u8glib team for their great work.


IMPORTANT LICENSING INFORMATION
-------------------------------
This code is the property of J. Terblanche, and is licensed under the terms of
the GNU General Public License (GPL). See license.txt for more information.

Please take note that the ST and u8glib libraries may use a different license
and are not bound/covered by this license in any way.


USING THIS LIBRARY
------------------
1. Interface Support
Currently only SPI mode is implemented. Support for I2C and/or other interfaces
may be added later, but this is not guaranteed.

2. Processor Support
Currently only the SPI interfaces for F10x and F40x series MCU's are implemented. 
There is currently no plan to support any other MCU's in the STM32 product line.

THIS CODE IS NOT SUPPORTED IN ANY WAY WHATSOEVER. 


IMPLEMENTATION STEPS
--------------------
1. Import the library into your project.

2. Customize the parameters in u8g_stm32_helper.h to suit your requirements.

3. Provide a function that performs a 1 microsecond delay. 
   The delay function prototype is as follows:
   
       void Delay_Milliseconds(__IO uint32_t nTime);
       
4. If you want u8glib to control power to your display device, enable 
   U8G_USE_POWER_CONTROL_FUNCTIONS in u8g_stm32_helper.h by setting 
   the value to 'true', and provide the following two functions.
   
   		void U8G_Device_PowerDown();
		void U8G_Device_PowerUp();
		
	These two functions should implement the complete functionality required
	to power up/down your device. 
		


