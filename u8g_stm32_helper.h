/*

  @file
  @author J. Terblanche
  @version 1.0.0

  @section DESCRIPTION
  u8g_stm32_helper.h

  STM32 Port of the hardware abstraction functions of the u8glib graphics library.

  NOTES:
  	  See the readme.txt file for implementation notes.

  @section LICENSE

  Copyright (c) 2013, J. Terblanche.
  Portions Copyright (c) 2011, olikraus@gmail.com
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice, this list
    of conditions and the following disclaimer.

  * Redistributions in binary form must reproduce the above copyright notice, this
    list of conditions and the following disclaimer in the documentation and/or other
    materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


*/


#ifndef U8G_STM32_HELPER_H_
#define U8G_STM32_HELPER_H_

#include "u8g.h"
#include <stdbool.h>
#include <stdint.h>

#define U8G_USE_POWER_CONTROL_FUNCTIONS 		false

#if defined (STM32F40_41xxx)
	#define U8GHELPER_SPI_GPIO_PORT 				GPIOB
	#define U8GHELPER_SPI_GPIO_CLOCKBIT 			RCC_AHB1Periph_GPIOB
	#define U8GHELPER_SPI_GPIO_CLOCKCMD 			RCC_AHB1PeriphClockCmd
	#define U8GHELPER_SPI_CS_PIN 					GPIO_Pin_12
	#define U8GHELPER_SPI_CS_PINSOURCE				GPIO_PinSource12
	#define U8GHELPER_SPI_CLK_PIN 					GPIO_Pin_13
	#define U8GHELPER_SPI_CLK_PINSOURCE				GPIO_PinSource13
	#define U8GHELPER_SPI_MOSI_PIN 					GPIO_Pin_15
	#define U8GHELPER_SPI_MOSI_PINSOURCE			GPIO_PinSource15
	#define U8GHELPER_SPI_GPIO_AF					GPIO_AF_SPI2

	#define U8GHELPER_SPI_PERIPH					SPI2
	#define U8GHELPER_SPI_CLOCKBIT					RCC_APB1Periph_SPI2
	#define U8GHELPER_SPI_CLOCKCMD					RCC_APB1PeriphClockCmd

	#define U8GHELPER_RESET_GPIO_PORT 				GPIOD
	#define U8GHELPER_RESET_GPIO_CLOCKBIT 			RCC_AHB1Periph_GPIOD
	#define U8GHELPER_RESET_GPIO_CLOCKCMD 			RCC_AHB1PeriphClockCmd
	#define U8GHELPER_RESET_PIN 					GPIO_Pin_10

	#define U8GHELPER_DNC_GPIO_PORT 				GPIOD
	#define U8GHELPER_DNC_GPIO_CLOCKBIT 			RCC_AHB1Periph_GPIOD
	#define U8GHELPER_DNC_GPIO_CLOCKCMD 			RCC_AHB1PeriphClockCmd
	#define U8GHELPER_DNC_PIN 						GPIO_Pin_9
#else
	#define U8GHELPER_SPI_GPIO_PORT 				GPIOA
	#define U8GHELPER_SPI_GPIO_CLOCKBIT 			RCC_APB2Periph_GPIOA
	#define U8GHELPER_SPI_GPIO_CLOCKCMD 			RCC_APB2PeriphClockCmd
	#define U8GHELPER_SPI_CS_PIN 					GPIO_Pin_4
	#define U8GHELPER_SPI_CLK_PIN 					GPIO_Pin_5
	#define U8GHELPER_SPI_MOSI_PIN 					GPIO_Pin_7

	#define U8GHELPER_SPI_PERIPH					SPI1
	#define U8GHELPER_SPI_CLOCKBIT					RCC_APB2Periph_SPI1
	#define U8GHELPER_SPI_CLOCKCMD					RCC_APB2PeriphClockCmd

	#define U8GHELPER_RESET_GPIO_PORT 				GPIOA
	#define U8GHELPER_RESET_GPIO_CLOCKBIT 			RCC_APB2Periph_GPIOA
	#define U8GHELPER_RESET_GPIO_CLOCKCMD 			RCC_APB2PeriphClockCmd
	#define U8GHELPER_RESET_PIN 					GPIO_Pin_3

	#define U8GHELPER_DNC_GPIO_PORT 				GPIOA
	#define U8GHELPER_DNC_GPIO_CLOCKBIT 			RCC_APB2Periph_GPIOA
	#define U8GHELPER_DNC_GPIO_CLOCKCMD 			RCC_APB2PeriphClockCmd
	#define U8GHELPER_DNC_PIN 						GPIO_Pin_6
#endif

uint8_t u8g_com_hw_spi_fn(u8g_t *u8g, uint8_t msg, uint8_t arg_val, void *arg_ptr);

#endif /* U8G_STM32_HELPER_H_ */
